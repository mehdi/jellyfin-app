FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4


ARG VERSION
ARG JELLYFIN_RELEASE
LABEL maintainer="fzbasescu / CVE.random"


ARG DEBIAN_FRONTEND="noninteractive"
ENV NVIDIA_DRIVER_CAPABILITIES="compute,video,utility"
ENV JELLYFIN_DATA_DIR=/app/data/media
ENV JELLYFIN_CONFIG_DIR=/app/data/config
ENV JELLYFIN_CACHE_DIR=/run

RUN \
 apt-get update && \
 apt-get install -y --no-install-recommends \
	gnupg curl ca-certificates && \
 curl -o /tmp/key.gpg.key https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key && apt-key add /tmp/key.gpg.key && \
 echo 'deb [arch=amd64] https://repo.jellyfin.org/ubuntu bionic main' > /etc/apt/sources.list.d/jellyfin.list && \
 apt-get update && \
 apt-get install -y --no-install-recommends \
	at \
	i965-va-driver \
	jellyfin-ffmpeg \
	libfontconfig1 \
	libfreetype6 \
	libssl1.0.0 \
	mesa-va-drivers && \
 if [ -z ${JELLYFIN_RELEASE+x} ]; then \
	JELLYFIN_RELEASE=$(curl -sX GET "https://api.github.com/repos/jellyfin/jellyfin/releases/latest" \
	| awk '/tag_name/{print $4;exit}' FS='[""]'); \
 fi && \
 VERSION=$(echo "${JELLYFIN_RELEASE}" | sed 's/^v//g') && \
 curl -o \
 /tmp/jellyfin.deb -L \
	"https://github.com/jellyfin/jellyfin/releases/download/v${VERSION}/jellyfin_${VERSION}-1_ubuntu-amd64.deb" && \
 dpkg -i /tmp/jellyfin.deb && \
 rm -rf \
	/tmp/* \
	/var/lib/apt/lists/* \
	/var/tmp/*

COPY root/ / 
RUN mkdir -p /app/data/config && mkdir -p /app/data/media

EXPOSE 8096

CMD ["jellyfin"]

#!/usr/bin/env node

'use strict';

/* global describe */
/* global before */
/* global after */
/* global it */

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var LOCATION = 'test';
    var BOARD_NAME = 'test board cloudron ' + Math.floor((Math.random() * 100) + 1);
    var LIST_NAME = 'test list cloudron';
    var CARD_CONTENT = 'test card content cloudron';
    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    var server, browser = new Builder().forBrowser('chrome').build();
    var app;

    before(function () {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(identifier, callback) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn + '/sign-in');
        }).then(function () {
            return waitForElement(By.name('at-field-username'));
        }).then(function () {
            return browser.findElement(By.name('at-field-username')).sendKeys(identifier);
        }).then(function () {
            return browser.findElement(By.name('at-field-password')).sendKeys(process.env.PASSWORD);
        }).then(function () {
            // wait for login model sync
            return browser.sleep(1000);
        }).then(function () {
            return browser.findElement(By.id('at-btn')).click();
        }).then(function () {
            return waitForElement(By.className('js-add-board'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.sleep(3000);
        }).then(function () {
            return waitForElement(By.id('header-user-bar'));
        }).then(function () {
            return browser.findElement(By.id('header-user-bar')).click();
        }).then(function () {
            return waitForElement(By.className('js-logout'));
        }).then(function () {
            return browser.findElement(By.className('js-logout')).click();
        }).then(function () {
            return waitForElement(By.name('at-field-username_and_email'));
        }).then(function () {
            callback();
        });
    }

    function contentExists(callback) {
        const tag = app.manifest.version === '3.48.0' ? 'span' : 'p';
        browser.get('https://' + app.fqdn).then(function () {
            return browser.sleep(4000);
        }).then(function () {
            return waitForElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`));
        }).then(function () {
            return browser.findElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`)).click();
        }).then(function () {
            return waitForElement(By.xpath('//p[text()="' + CARD_CONTENT + '"]'));
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function createBoard(done) {
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(By.className('js-add-board'));
        }).then(function () {
            return browser.findElement(By.className('js-add-board')).click();
        }).then(function () {
            return waitForElement(By.className('js-new-board-title'));
        }).then(function () {
            return browser.findElement(By.className('js-new-board-title')).sendKeys(BOARD_NAME);
        }).then(function () {
            return browser.findElement(By.className('js-new-board-title')).sendKeys(Key.ENTER);
        }).then(function () { // creating a board changes view to the board immediately
            return waitForElement(By.className('list-name-input'));
        }).then(function () {
            setTimeout(done, 4000);
        });
    }

    function addList(done) {
        const tag = app.manifest.version === '3.48.0' ? 'span' : 'p';
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`));
        }).then(function () {
            return browser.findElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`)).click();
        }).then(function () {
            return waitForElement(By.className('list-name-input'));
        }).then(function () {
            return browser.findElement(By.className('list-name-input')).sendKeys(LIST_NAME);
        }).then(function () {
            return browser.findElement(By.className('list-name-input')).sendKeys(Key.ENTER);
        }).then(function () {
            return waitForElement(By.className('open-minicard-composer'));
        }).then(function () {
            setTimeout(done, 4000);
        });
    }

    function addCard(done) {
        const tag = app.manifest.version === '3.48.0' ? 'span' : 'p';
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`));
        }).then(function () {
            return browser.findElement(By.xpath(`//${tag}[text()="${BOARD_NAME}"]`)).click();
        }).then(function () {
            return waitForElement(By.className('open-minicard-composer'));
        }).then(function () {
            return browser.findElement(By.className('open-minicard-composer')).click();
        }).then(function () {
            return waitForElement(By.className('minicard-composer-textarea'));
        }).then(function () {
            return browser.findElement(By.className('minicard-composer-textarea')).sendKeys(CARD_CONTENT);
        }).then(function () {
            return browser.findElement(By.className('minicard-composer-textarea')).sendKeys(Key.ENTER);
        }).then(function () {
            return waitForElement(By.xpath('//p[text()="' + CARD_CONTENT + '"]'));
        }).then(function () {
            setTimeout(done, 4000);
        });
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, process.env.USERNAME));
    it('can create board', createBoard);
    it('can add list', addList);
    it('can add card', addCard);
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login with username', login.bind(null, process.env.USERNAME));
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () { execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS); });

    it('can login with username', login.bind(null, process.env.USERNAME));
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, process.env.USERNAME));
    it('board, list and card exists', contentExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('install app', function () { execSync(`cloudron install --appstore-id io.wekan.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login with username', login.bind(null, process.env.USERNAME));
    it('can create board', createBoard);
    it('can add list', addList);
    it('can add card', addCard);
    it('board, list and card exists', contentExists);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('board, list and card exists', contentExists);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
